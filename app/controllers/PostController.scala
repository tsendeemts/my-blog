package controllers

import play.api._
import play.api.mvc._
import views._
import models._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import org.joda.time.DateTime
import java.sql.Date
import jp.t2v.lab.play2.auth.AuthElement
import controllers.security.AuthConfigImpl

object PostController extends Controller with AuthElement with AuthConfigImpl {

  val postForm = Form(
    mapping(
      "id" -> optional(of[Int]),
      "title" -> nonEmptyText,
      "contents" -> nonEmptyText,
      "label" -> text,
      "categories" -> list(of[Int]),
      "draft" -> boolean)((id, title, contents, label, categories, draft) => PostEntry(id = id, title = title, contents = contents, label = label, categories = categories, draft = draft))((postEntry: PostEntry) => Some(postEntry.id, postEntry.title, postEntry.contents, postEntry.label, postEntry.categories, postEntry.draft)))

  def posts(page: Int, orderBy: Int, filter: String) = StackAction(AuthorityKey -> Application.allUser _) { implicit request =>
    Ok(views.html.posts(
      PostHelpers.list(page = page, orderBy = orderBy, filter = ("%" + filter + "%")),
      orderBy, filter))
  }

  def create() = StackAction(AuthorityKey -> Application.allUser _) { implicit request =>
    Ok(html.postForm(postForm, CategoryHelpers.idnames.toSeq))
  }

  def save() = StackAction(AuthorityKey -> Application.allUser _) { implicit request =>
    val user = loggedIn
    postForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.postForm(formWithErrors, CategoryHelpers.idnames.toSeq)),
      postEntry => {
        val post = Post(id = postEntry.id, title = postEntry.title, contents = postEntry.contents, label = postEntry.label, uid = user.id.get, draft = postEntry.draft)
        val id: Int = PostHelpers.insert(post)
        PostCategoryHelpers.insert(id, postEntry.categories)
        Redirect(routes.PostController.posts(0, 2, "")).flashing("success" -> "Post %s has been created".format(post.title))
      })
  }

  def edit(id: Int) = StackAction(AuthorityKey -> Application.isOwnerOrAdmin(id) _) { implicit request =>
    val user = loggedIn
    Ok(html.postFormEdit(id, postForm.fill(PostHelpers.findByIdWithPostEntry(id)), CategoryHelpers.idnames.toSeq))
  }

  def update(id: Int) = StackAction(AuthorityKey -> Application.isOwnerOrAdmin(id) _) { implicit request =>
    postForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.postFormEdit(id, formWithErrors, CategoryHelpers.idnames.toSeq)),
      postEntry => {
        //TODO: Take care of user, and categories
        val post = Post(id = postEntry.id, title = postEntry.title, contents = postEntry.contents, label = postEntry.label, draft = postEntry.draft)
        PostHelpers.update(post)
        PostCategoryHelpers.deleteByPostId(id)
        PostCategoryHelpers.insert(id, postEntry.categories)
        Redirect(routes.PostController.posts(0, 2, "")).flashing("success" -> "Post %s has been modified".format(post.title))
      })
  }

  def delete(id: Int) = StackAction(AuthorityKey -> Application.isOwnerOrAdmin(id) _) { implicit request =>
    PostHelpers.deleteById(id)
    PostCategoryHelpers.deleteByPostId(id)
    Redirect(routes.PostController.posts(0, 2, "")).flashing("success" -> "Post has been deleted")
  }
}