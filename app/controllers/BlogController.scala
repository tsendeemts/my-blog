package controllers

import play.api._
import play.api.mvc._
import views._
import models._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import org.joda.time.DateTime
import java.sql.Date
import utils.PasswordHashing
import play.api.cache.Cached
import play.api.Play.current
import play.api.cache.Cache

object BlogController extends Controller {

  def blog(page: Int = 0, filter: String = "") = Cached("homePage", 6000) {
    Action { implicit request =>
      // Ok(views.html.index("Welcome!", "posts")(play.api.templates.Html("")))
      Ok(html.blog.posts(
        PostHelpers.postViews(page = page, filter = ("%" + filter + "%")), filter, recentPosts = recentPosts))
    }
  }

  def blogPage(page: Int = 0, filter: String = "") = Action { implicit request =>
    // Ok(views.html.index("Welcome!", "posts")(play.api.templates.Html("")))

    if (filter != "") {
      val msg = "Search: %s".format(filter)
      Ok(html.blog.posts(
        PostHelpers.postViewsByTitle(filter = ("%" + filter + "%")), filter, msg, recentPosts = recentPosts))
    } else {
      Ok(html.blog.posts(
        PostHelpers.postViews(page = page, filter = ("%" + filter + "%")), filter, recentPosts = recentPosts))
    }
  }

  def post(pid: Int, title: String) = Action { implicit request =>
    Ok(html.blog.postEntry(
      PostHelpers.postView(pid), recentPosts = recentPosts))
  }

  def postByLabel(cid: Int, cname: String) = Action { implicit request =>
    Ok(html.blog.posts(
      PostHelpers.postViewsByCategory(cid), "", "Label archive: %s".format(CategoryHelpers.findById(cid).cname), recentPosts = recentPosts))
  }

  def postByAuthor(id: Int, dname: String) = Action { implicit request =>
    Ok(html.blog.posts(
      PostHelpers.postViewsByAuthor(id), "", "Author archive: %s".format(dname), recentPosts = recentPosts))
  }

  def publications() = Action { implicit request =>
    Application.notYet
  }

  def srccode() = Action { implicit request =>
    Application.notYet
  }

  def me() = Cached("me", 6000) {
    Action { implicit request =>
      Ok(html.blog.me())
    }
  }

  def archive() = Action { implicit request =>
    Ok(html.blog.archive(
        PostHelpers.postArchive(), "Full archive", recentPosts = recentPosts))
  }
  
  def recentPosts() = {
    Cache.getOrElse("recentPosts") {
    	val posts = PostHelpers.recentPosts()
    	// cashe it for 100mins
    	Cache.set("recentPosts", posts, 6000)
    	posts
    }
  }
}