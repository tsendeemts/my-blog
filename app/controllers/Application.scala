package controllers

import play.api._
import play.api.mvc._
import views._
import models._
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import org.joda.time.DateTime
import java.sql.Date
import jp.t2v.lab.play2.auth._
import security._
import org.mindrot.jbcrypt.BCrypt
import utils.PasswordHashing
import views.html.defaultpages.badRequest

object Application extends Controller with AuthElement with AuthConfigImpl {

  var userId: Int = 1

  val userForm = Form(
    mapping(
      "id" -> optional(of[Int]),
      "fname" -> nonEmptyText,
      "lname" -> nonEmptyText,
      "uname" -> text(minLength = 4),
      //"pass" -> nonEmptyText,
      "pass" -> tuple(
        "main" -> text(minLength = 6),
        "confirm" -> nonEmptyText).verifying(
          "Passwords don't match", passwords => passwords._1 == passwords._2),
      "dname" -> nonEmptyText,
      "email" -> email.verifying("User email exists", email => UserHelpers.findByEmail(email).isEmpty))((id, fname, lname, uname, pass, dname, email) => User(id, fname, lname, uname, pass._1, dname, new Date(System.currentTimeMillis()), email))((user: User) => Some(user.id, user.fname, user.lname, user.uname, (user.pass, user.pass), user.dname, user.email)))

  val userFormEdit = Form(
    mapping(
      "id" -> optional(of[Int]),
      "fname" -> nonEmptyText,
      "lname" -> nonEmptyText,
      "uname" -> text(minLength = 4),
      "dname" -> nonEmptyText,
      "email" -> email)((id, fname, lname, uname, dname, email) => User(id = id, fname = fname, lname = lname, uname = uname, dname = dname, email = email))((user: User) => Some(user.id, user.fname, user.lname, user.uname, user.dname, user.email)))

  val passwordForm = Form(
    mapping(
      "id" -> optional(of[Int]),
      "oldpass" -> text(minLength = 6),
      "pass" -> tuple(
        "main" -> text(minLength = 6),
        "confirm" -> text).verifying(
          "Passwords don't match", passwords => passwords._1 == passwords._2 // TODO: check if oldpass is right
          ))((id, oldpass, pass) => User(id = id, pass = pass._1))((user: User) => Some(user.id, user.pass, ("", ""))))

  def isOwnerOrAdmin(postId: Int)(user: User): Boolean = {
    if (user.dname == "tsendee") {
      true
    } else {
      PostHelpers.findById(postId).uid == user.id.get
    }
  }

  def isAdmin(user: User): Boolean = {
    user.dname == "tsendee"
  }

  def allUser(user: User): Boolean = true

  val categoryForm = Form(
    mapping(
      "id" -> optional(of[Int]),
      "cname" -> nonEmptyText,
      "pid" -> optional(of[Int]))(Category.apply)(Category.unapply))

  def users(page: Int, orderBy: Int, filter: String) = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    Ok(views.html.users(
      Users.list(page = page, orderBy = orderBy, filter = ("%" + filter + "%")),
      orderBy, filter))
  }

  def createuser() = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    Ok(html.userForm(userForm))
  }

  def saveuser() = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    userForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.userForm(formWithErrors)),
      user => {
        //        val encryptedPassword = PasswordHashing.encryptPassword(user.pass)
        val encryptedPassword = BCrypt.hashpw(user.pass, BCrypt.gensalt())
        val newUser = User(user.id, user.fname, user.lname, user.uname, encryptedPassword, user.dname, user.cdate, user.email)
        UserHelpers.insert(newUser)
        Redirect(routes.Application.users(0, 2, "")).flashing("success" -> "User %s has been created".format(user.fname))
      })
  }

  def edituser(id: Int) = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    val user = UserHelpers.findById(id)
    if (user.isDefined) {
      Ok(html.userFormEdit(id, userFormEdit.fill(user.get)))
    } else {
      BadRequest("User not found")
    }
  }

  def updateuser(id: Int) = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    userFormEdit.bindFromRequest.fold(
      formWithErrors => BadRequest(html.userFormEdit(id, formWithErrors)),
      user => {
        if (UserHelpers.checkEmailDuplication(user.email, id).isEmpty) {
          UserHelpers.update(user)
          Redirect(routes.Application.users(0, 2, "")).flashing("success" -> "User %s has been modified".format(user.fname))
        } else {
          Ok(html.userFormEdit(id, userForm.fill(user)))
        }
      })
  }

  def deleteuser(id: Int) = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    UserHelpers.deleteById(id)
    Redirect(routes.Application.users(0, 2, "")).flashing("success" -> "User has been deleted")
  }

  def categories(page: Int, orderBy: Int, filter: String) = StackAction(AuthorityKey -> allUser _) { implicit request =>
    Ok(views.html.categories(
      CategoryHelpers.list(page = page, orderBy = orderBy, filter = ("%" + filter + "%")),
      orderBy, filter, CategoryHelpers.idnames))
  }

  def createcategory() = StackAction(AuthorityKey -> allUser _) { implicit request =>
    Ok(html.categoryForm(categoryForm, CategoryHelpers.idnames.toSeq))
  }

  def savecategory() = StackAction(AuthorityKey -> allUser _) { implicit request =>
    categoryForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.categoryForm(formWithErrors, CategoryHelpers.idnames.toSeq)),
      category => {
        CategoryHelpers.insert(category)
        Redirect(routes.Application.categories(0, 2, "")).flashing("success" -> "Category %s has been created".format(category.cname))
      })
  }

  def editcategory(id: Int) = StackAction(AuthorityKey -> allUser _) { implicit request =>
    Ok(html.categoryFormEdit(id, categoryForm.fill(CategoryHelpers.findById(id)), CategoryHelpers.idnames.toSeq))
  }

  def updatecategory(id: Int) = StackAction(AuthorityKey -> allUser _) { implicit request =>
    categoryForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.categoryFormEdit(id, formWithErrors, CategoryHelpers.idnames.toSeq)),
      category => {
        CategoryHelpers update category
        Redirect(routes.Application.categories(0, 2, "")).flashing("success" -> "Category %s has been modified".format(category.cname))
      })
  }

  def deletecategory(id: Int) = StackAction(AuthorityKey -> isAdmin _) { implicit request =>
    CategoryHelpers.deleteById(id)
    Redirect(routes.Application.categories(0, 2, "")).flashing("success" -> "Category has been deleted")
  }

  def comments = StackAction(AuthorityKey -> allUser _) { implicit request =>
    Ok(views.html.comments())
  }

  def notYet = Ok("Not implemented yet!")
}