package models

import scala.slick.driver.MySQLDriver.simple._
import java.sql.Date
import play.api._
import play.api.mvc._
import scala.slick.session.Session
import play.api.db._
import play.api.Play.current
import Database.threadLocalSession
import org.mindrot.jbcrypt.BCrypt

case class User(id: Option[Int] = Option(1), fname: String = "", lname: String = "", uname: String = "", pass: String = "", dname: String = "", cdate: Date = new Date(System.currentTimeMillis()), email: String = "")

object Users extends Table[User]("users") {

  lazy val database = Database.forDataSource(DB.getDataSource())

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def fname = column[String]("fname")
  def lname = column[String]("lname")
  def uname = column[String]("uname", O.NotNull)
  def unamex = index("unamex", uname, unique = true)
  def pass = column[String]("pass", O.NotNull)
  def dname = column[String]("dname", O.NotNull)
  def dnamex = index("dnamex", dname, unique = true)
  def cdate = column[Date]("cdate", O.Nullable, O.Default(new Date(System.currentTimeMillis())))
  def email = column[String]("email", O.NotNull)
  def emailx = index("emailx", email, unique = true)

  def * = id.? ~ fname ~ lname ~ uname ~ pass ~ dname ~ cdate ~ email <> (User, User.unapply _)

  def cols = Vector(id, fname, lname, uname, pass, dname, cdate, email)

  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[User] = {
    val offset = pageSize * page

    val ret = database withSession {
      var q1 = Query(Users)
      val q = Query(Users)
      if (orderBy < 0) {
        q1 = q.filter(_.fname like filter).sortBy(_.cols(-(orderBy)).desc.nullsLast)
      } else {
        q1 = q.filter(_.fname like filter).sortBy(_.cols(orderBy).asc.nullsLast)
      }
      val users = q1.drop(offset).take(pageSize).list
      val totalRows = Query(q1.length).first
      Page(users, page, offset, totalRows)
    }
    ret
  }

}

object UserHelpers {
  lazy val database = Database.forDataSource(DB.getDataSource())

  def authenticate(email: String, password: String): Option[User] = {
//    findByEmail(email).filter { user => BCrypt.checkpw(password, account.password) }
    findByEmail(email).filter { user => BCrypt.checkpw(password, user.pass) }
  }

  def insert(user: User) = {
    database withSession {
      (Users insert user)
    }
  }

  def findById(id: Int) = {
    database withSession {
      val q = for {
        u <- Users if u.id === id
      } yield (u)
      q.firstOption
    }
  }

  def update(user: User) = {
    database withSession {
      user match {
        case User(id, fname, lname, uname, "", dname, _, email) => {
          (for {
            u <- Users if u.id === id
          } yield (u.fname ~ u.lname ~ u.uname ~ u.dname ~ u.email)).update(fname, lname, uname, dname, email)
        } // update user only info
        case User(id, "", "", "", pass, "", _, "") => {
          (for {
            u <- Users if u.id === id
          } yield (u.pass)).update(pass)
        } // update only password
        case _ => println("Wrong value to update")
      }
    }
  }

  def findByEmail(email: String): Option[User] = {
    database withSession {
      (for (u <- Users if u.email === email) yield (u)).firstOption
    }
  }

  def checkEmailDuplication(email: String, id: Int) = {
    database withSession {
      Users.filter(u => ((u.id =!= id) && (u.email === email))).list
    }
  }

  def deleteById(id: Int) = {
    database withSession {
      (for (u <- Users if u.id === id) yield (u)).delete
    }
  }

}