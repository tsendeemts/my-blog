package models

import play.api._
import play.api.mvc._

import play.api.data._

import scala.slick.driver.MySQLDriver.simple._

import scala.slick.session.Session
 
import play.api.db._
import play.api.Play.current

import Database.threadLocalSession

/**
 * Helper for pagination.
 */
case class Page[A](items: Seq[A], page: Int, offset: Long, total: Long) {
  lazy val prev = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
}

object Helper{
	
//	def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%", tb: ): Page[Post] = {
//    val offset = pageSize * page
//    
//    val ret = database withSession {
//      val q = Query(Posts)
//      val q1 = q.filter(_.title like filter).sortBy(_.cols(orderBy).asc.nullsLast)
//      val posts = q1.drop(offset).take(pageSize).list
//      val totalRows = Query(q1.length).first
//      
//      Page(posts, page, offset, totalRows)
//    }
//    
//    ret
//  }
}


