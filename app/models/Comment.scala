package models

import scala.slick.driver.MySQLDriver.simple._
import java.sql.Date

case class Comment (id: Option[Int], comment: String, name: String, email: String, ipaddress: String, cdate: Date, postid: Int)

object Comments extends Table[Comment]("comments"){
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def comment = column[String]("comment", O.NotNull)
  def name = column[String]("name")
  def email = column[String]("email")
  def ipaddress = column[String]("ipaddress", O.DBType("VARCHAR(20)"))
  def cdate = column[Date]("cdate", O.Default[Date](new Date(System.currentTimeMillis())))
  def postid = column[Int]("postid")
  
  def post = foreignKey("postid_fk", postid, Posts)(_.id)
  
  def * = id.? ~ comment ~ name ~ email ~ ipaddress ~ cdate ~ postid<> (Comment, Comment.unapply _)
}