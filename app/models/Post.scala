package models

import scala.slick.driver.MySQLDriver.simple._
import java.sql.Date

import play.api._
import play.api.mvc._

import scala.slick.session.Session

import play.api.db._
import play.api.Play.current

import Database.threadLocalSession

case class Post(id: Option[Int] = Option(1), title: String = "", contents: String = "", cdate: Date = new Date(System.currentTimeMillis()), mdate: Date = new Date(System.currentTimeMillis()), label: String = "", uid: Int = 1, draft: Boolean = false)

case class PostEntry(id: Option[Int], title: String, contents: String, label: String, categories: List[Int], draft: Boolean = false)

case class PostView(post: Post, categories: List[Category], user: (String, String))

object Posts extends Table[Post]("posts") {

  lazy val database = Database.forDataSource(DB.getDataSource())

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title = column[String]("title", O.NotNull)
  def contents = column[String]("contents", O.NotNull, O.DBType("MEDIUMTEXT"))
  def cdate = column[Date]("cdate", O.Default(new Date(System.currentTimeMillis())))
  def mdate = column[Date]("mdate", O.Default(new Date(System.currentTimeMillis())))
  def label = column[String]("label")
  def uid = column[Int]("uid", O.Default(1))
  def draft = column[Boolean]("draft", O.Default(false))

  def user = foreignKey("uid_fk", uid, Users)(_.id)

  def cols = Vector(id, title, contents, cdate, mdate, label, uid, draft)

  def * = id.? ~ title ~ contents ~ cdate ~ mdate ~ label ~ uid ~ draft <> (Post, Post.unapply _)

}

object PostHelpers {
  lazy val database = Database.forDataSource(DB.getDataSource())

  def postViews(page: Int = 0, pageSize: Int = 3, filter: String = "%"): Page[PostView] = {
    val offset = pageSize * page
    database withSession {
      var q1 = Query(Posts)
      val q = filterByDraft()
      q1 = q.filter(_.title like filter).sortBy(_.mdate.desc.nullsLast)
      val posts = q1.drop(offset).take(pageSize).list
      val totalRows = Query(q1.length).first
      val postViews = posts.map { post =>
        PostView(post, PostCategoryHelpers.findCategoriesByPostIdWithJoin(post.id.get), (post.uid.toString, UserHelpers.findById(post.uid).get.dname))
      }
      Page(postViews, page, offset, totalRows)
    }
  }
  
  def postViewsByTitle(filter: String = "%"): Page[PostView] = {
    database withSession {
      var q1 = Query(Posts)
      val q = filterByDraft()
      q1 = q.filter(_.title like filter).sortBy(_.mdate.desc.nullsLast)
      val posts = q1.list
      val postViews = posts.map { post =>
        PostView(post, PostCategoryHelpers.findCategoriesByPostIdWithJoin(post.id.get), (post.uid.toString, UserHelpers.findById(post.uid).get.dname))
      }
      Page(postViews, 0, 0, postViews.size)
    }
  }

  def postArchive(): Page[PostView] = {
    database withSession {
      var q1 = Query(Posts)
      val q = filterByDraft()
      q1 = q.sortBy(_.mdate.desc.nullsLast)
      val posts = q1.list
      val postViews = posts.map { post =>
        PostView(post, PostCategoryHelpers.findCategoriesByPostIdWithJoin(post.id.get), (post.uid.toString, UserHelpers.findById(post.uid).get.dname))
      }
      Page(postViews, 0, 0, postViews.size)
    }
  }
  
  def recentPosts(n: Int = 5) = {
    database withSession{
      val q = filterByDraft()
      val recentPosts = q.sortBy(_.mdate.desc.nullsLast).take(n)
      (for {
          p <- recentPosts
        } yield (p.id, p.title)).list
    }
  }

  def filterByDraft(draft: Boolean = false) = {
    database withSession {
      for {
        p <- Posts if p.draft === draft
      } yield (p)
    }
  }

  def postView(pid: Int): PostView = {
    database withSession {
      val post = findById(pid)
      PostView(post, PostCategoryHelpers.findCategoriesByPostIdWithJoin(post.id.get), (post.uid.toString, UserHelpers.findById(post.uid).get.dname))
    }
  }

  def postViewsByCategory(cid: Int) = {
    database withSession {
      val posts = (for {
        (p, pc) <- filterByDraft() innerJoin PostCategories on (_.id === _.pid) if pc.cid === cid
      } yield (p)).list
      val postViews = posts.map { post =>
        PostView(post, PostCategoryHelpers.findCategoriesByPostIdWithJoin(post.id.get), (post.uid.toString, UserHelpers.findById(post.uid).get.dname))
      }
      Page(postViews, 0, 0, postViews.size)
    }
  }

  def postViewsByAuthor(uid: Int) = {
    database withSession {
      val posts = (for {
        p <- filterByDraft() if p.uid === uid
      } yield (p)).list
      val postViews = posts.map { post =>
        PostView(post, PostCategoryHelpers.findCategoriesByPostIdWithJoin(post.id.get), (post.uid.toString, UserHelpers.findById(post.uid).get.dname))
      }
      Page(postViews, 0, 0, postViews.size)
    }
  }

  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[Post] = {
    val offset = pageSize * page
    val ret = database withSession {
      var q1 = Query(Posts)
      val q = Query(Posts)
      if (orderBy < 0) {
        q1 = q.filter(_.title like filter).sortBy(_.cols(-(orderBy)).desc.nullsLast)
      } else {
        q1 = q.filter(_.title like filter).sortBy(_.cols(orderBy).asc.nullsLast)
      }
      val posts = q1.drop(offset).take(pageSize).list
      val totalRows = Query(q1.length).first
      Page(posts, page, offset, totalRows)
    }
    ret
  }

  def insert(post: Post) = {
    database withSession {
      (Posts returning Posts.id insert post)
    }
  }

  def findById(id: Int) = {
    database withSession {
      (for (p <- Posts if p.id === id) yield (p)).list.head
    }
  }

  def findByIdWithPostEntry(id: Int) = {
    database withSession {
      val post = (for (p <- Posts if p.id === id) yield (p)).list.head
      val postIds = PostCategoryHelpers.findByPostId(id)
      PostEntry(post.id, post.title, post.contents, post.label, postIds, post.draft)
    }
  }

  def update(post: Post) = {
    database withSession {
      (for {
        p <- Posts if p.id === post.id
      } yield (p.title ~ p.contents ~ p.mdate ~ p.label ~ p.uid ~ p.draft)).update(post.title, post.contents, post.mdate, post.label, post.uid, post.draft)
    }
  }

  def deleteById(id: Int) = {
    database withSession {
      (for (p <- Posts if p.id === id) yield (p)).delete
    }
  }
}

case class PostCategory(pid: Int, cid: Int)

object PostCategories extends Table[PostCategory]("postcategories") {
  def pid = column[Int]("pid", O.NotNull)
  def cid = column[Int]("cid", O.NotNull)

  def pk = primaryKey("pk_a", (pid, cid))

  def * = pid ~ cid <> (PostCategory, PostCategory.unapply _)
}

object PostCategoryHelpers {
  lazy val database = Database.forDataSource(DB.getDataSource())

  def insert(pid: Int, cids: List[Int]) = {
    database withSession {
      PostCategories.insertAll(
        (for (cid <- cids) yield (PostCategory(pid, cid))): _*)
    }
  }

  def findCategoriesByPostIdWithJoin(pid: Int) = {
    database withSession {
      (for {
        (c, pc) <- Categories innerJoin PostCategories on (_.id === _.cid) if pc.pid === pid
      } yield (c)).list
    }
  }

  def findByPostId(pid: Int) = {
    database withSession {
      (for (pc <- PostCategories if pc.pid === pid) yield (pc.cid)).list
    }
  }

  def findCategoriesByPostId(pid: Int) = {
    database withSession {
      (for (pc <- PostCategories if pc.pid === pid) yield (pc.cid)).list
    }
  }

  def deleteByPostId(pid: Int) = {
    database withSession {
      (for (pc <- PostCategories if pc.pid === pid) yield (pc)).delete
    }
  }
}