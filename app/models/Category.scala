package models

import scala.slick.driver.MySQLDriver.simple._
import java.sql.Date
import play.api._
import play.api.mvc._
import scala.slick.session.Session
import play.api.db._
import play.api.Play.current
import Database.threadLocalSession

case class Category (id: Option[Int] = Option(1), cname: String = "", pid: Option[Int] = Option(0))

object Categories extends Table[Category]("categories"){
  
  lazy val database = Database.forDataSource(DB.getDataSource())
  
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def cname = column[String]("cname", O.NotNull)
  def pid = column[Int]("pid", O.Default(0), O.Nullable)

  // def pcategory = foreignKey("pid_fk", pid, Categories)(_.id)
  
  def * = id.? ~ cname ~ pid.? <> (Category, Category.unapply _)
  
  def cols = Vector(id, cname, pid)

}

object CategoryHelpers {
	lazy val database = Database.forDataSource(DB.getDataSource())

	def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[Category] = {
	  	val offset = pageSize * page
	  
	  	val ret = database withSession {
	    var q1 = Query(Categories)
	    val q = Query(Categories)
	    if (orderBy < 0){
	      q1 = q.filter(_.cname like filter).sortBy(_.cols(-(orderBy)).desc.nullsLast)
	    }else{
	      q1 = q.filter(_.cname like filter).sortBy(_.cols(orderBy).asc.nullsLast)
	    }
	    val categories = q1.drop(offset).take(pageSize).list
	    val totalRows = Query(q1.length).first
	    Page(categories, page, offset, totalRows)
	  }
	  ret
	}

	def findById(id: Int) = {
	  database withSession {
	    val q = for {
	      c <- Categories if c.id === id
	    } yield (c)
	    q.list.head
	  }
	}

	def idnames(): Map[String, String] = {
		database withSession{
			Query(Categories).list.map(c => (c.id.get.toString -> c.cname)).toMap
		}
	}

	def insert(category: Category) = {
		database withSession{
			Categories insert category
		}
	}

	def update(category: Category) = {
		database withSession{
			(for{
	            c <- Categories if c.id === category.id
	          	} yield (c)
	        ).update(category)
		}
	}

	def deleteById(id: Int) = {
		database withSession{
			(for( c <- Categories if c.id === id) yield (c)).delete
		}
	}
}