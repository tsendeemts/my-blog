import play.api.db.DB
import play.api.GlobalSettings
import scala.slick.driver.MySQLDriver.simple._
import Database.threadLocalSession
import play.api.Application
import play.api.Play.current
import models._
import play.api.mvc._
import Results._
import play.api.Logger

object Global extends GlobalSettings {

  override def onStart(app: Application) {

    lazy val database = Database.forDataSource(DB.getDataSource())
//
//    val ddl = models.Posts.ddl ++ models.Categories.ddl ++ models.Comments.ddl ++
//      models.Users.ddl ++ models.PostCategories.ddl

    // // database.withSession{
    // // 	ddl.drop
    // // }

    //    database.withSession {
    //      ddl.create
    //      
    //    }
  }

  override def onHandlerNotFound(request: RequestHeader) = {
    Logger.error("Page Not Found - " + request.path)
    NotFound(views.html.errorPage("Page Not Found", "The page is not here"))
  }

  override def onBadRequest(request: RequestHeader, error: String) = {
    Logger.error("Page Not Found (BadRequest) - " + request.path)
    BadRequest(
      views.html.errorPage("BadRequest", "Page Not Found - " + request.path + " Error " + error))
  }

  override def onError(request: RequestHeader, ex: Throwable) = {
    Logger.error("Error occurred", ex)
    InternalServerError(
      views.html.serverError("Internal server error", "oops ! Some Error Occured"))
  }
}