Font license info


## Zocial

   Copyright (C) 2012 by Sam Collins

   Author:    Sam Collins
   License:   MIT (http://opensource.org/licenses/mit-license.php)
   Homepage:  http://zocial.smcllns.com/


## MFG Labs

   Copyright (C) 2012 by Daniel Bruce

   Author:    MFG Labs
   License:   SIL (http://scripts.sil.org/OFL)
   Homepage:  http://www.mfglabs.com/


## Font Awesome

   Copyright (C) 2012 by Dave Gandy

   Author:    Dave Gandy
   License:   CC BY 3.0 (http://creativecommons.org/licenses/by/3.0/)
   Homepage:  http://fortawesome.github.com/Font-Awesome/


## Entypo

   Copyright (C) 2012 by Daniel Bruce

   Author:    Daniel Bruce
   License:   CC BY-SA (http://creativecommons.org/licenses/by-sa/2.0/)
   Homepage:  http://www.entypo.com


